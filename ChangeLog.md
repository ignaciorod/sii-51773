
# ChangeLog
Todos los cambios notables del proyecto se documentarán aquí:

# v3.1 [19-1-2019]
## Añadido
	- Clase DatosMemCompartida que tiene la información de la esfera, la raqueta
	y la accion de control
	- Programa bot (bot.cpp) que toma el control del jugador2. El bot lee de una proyeccion en memoria los datos de la pelota y el jugador y
	en un bucle actualiza la accion de control (int accion) del jugador.Esta accion de control se lee en Mundo y se hace un OnKeyboardDown.
	- Mundo crea una proyeccion en memoria (mmap()) de un fichero donde se escribe
	un atributo	de tipo DatosMemCompartida. En ese fichero se escriben y leen los
	dat

## Modificado
	- CMakeLists.txt actualizado para crear el ejecutable bot

## Eliminado
	- No se multiplica el numero de esferas a medida que pasa el juego para hacer
	más fácil el control del bot.



# v3.0 [18-11-2019]
## Añadido
	- Programa Logger (logger.cpp) que imprime por pantalla los puntos de los jugadores
	- Logger crea una FIFO en tmp/myfifo y recibe datos de tenis
	- tenis abre el fichero /tmp/myfifo y escribe en este, comunicandose mediante una FIFO con Logger

## Modificado
 	- Mundo lleva un recuento de los puntos y mediante un flag tenis los logea
	- CMakeLists.txt actualizado para crear el ejecutable logger

# v2.1 [04-11-2019]
## Añadido
	- Interacción entre Disparo y Raqueta.
	- La esfera se divide cada 5 puntos.
	- La esfera disminuye su tamaño cada 3 puntos.
	- La clase Disparo implemneta su propio método Dibuja
	- Al impactar un disparo con la raqueta oponente, esta
	disminuye un 20% su tamaño.

## Modificado
	- Las esferas se crean dinamicamente dentro de una lista
	ListaEsferas.

# v2.0	[22-10-2019]
## Añadido
	- Clase Disparo que hereda de Esfera. Se instancia desde una
		lista "ListaDisparos" en CMundo

## Borrado
	- Directorio /build

## Modificado
	- Archivo ChangeLog al directorio principal


# v1.2  [09-10-2019]
## Añadido
	- Movimiento a la esfera y los jugadores


# v1.1	[08-10-2019]
## Añadido
	- Directorio /build
