// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	points_flag = false;
	Init();
}

CMundo::~CMundo()
{
	for(int i = 0; i < ListaDisparos.size(); i++) { delete ListaDisparos[i];	}
	for(int i = 0; i < ListaEsferas.size(); i++) { delete ListaEsferas[i];	}

	//for_each(ListaDisparos->begin(), ListaDisparos->end(), delete);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	for(int i = 0; i < ListaDisparos.size(); i++){
		ListaDisparos[i]->Dibuja();
	}

	for(int i = 0; i < ListaEsferas.size(); i++){
		ListaEsferas[i]->Dibuja();
	}

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	//esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);

	for (int i = 0; i < ListaEsferas.size(); i++){
		ListaEsferas[i]->Mueve(0.025f);
	}

	for (int i = 0; i < ListaDisparos.size(); i++){
		ListaDisparos[i]->Mueve(0.025f);
	}

	for (int i = 0; i < ListaDisparos.size(); i++){
		if(jugador1.Rebota(*ListaDisparos[i]) && ListaDisparos[i]->id == 2){
			jugador1.Shrink();
			ListaDisparos.erase(ListaDisparos.begin() + i);
		}

		if(jugador2.Rebota(*ListaDisparos[i]) && ListaDisparos[i]->id == 1){
			jugador2.Shrink();
			ListaDisparos.erase(ListaDisparos.begin() + i);
		}
	}

	for(int i = 0; i < paredes.size(); i++)
	{
		for (int j = 0; j < ListaEsferas.size(); j++){
			paredes[i].Rebota(*ListaEsferas[j]);
		}
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	for(int j = 0; j < ListaEsferas.size(); j++){
		jugador1.Rebota(*ListaEsferas[j]);
		jugador2.Rebota(*ListaEsferas[j]);
	}

	int aux = puntos1 + puntos2;	// variable for keeping track of the change in points

	for (int j = 0; j < ListaEsferas.size(); j++){

		if(fondo_izq.Rebota(*ListaEsferas[j]))
		{
			ListaEsferas[j]->centro.x=0;
			ListaEsferas[j]->centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[j]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			ListaEsferas[j]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
		}

		if(fondo_dcho.Rebota(*ListaEsferas[j]))
		{
			ListaEsferas[j]->centro.x=0;
			ListaEsferas[j]->centro.y=rand()/(float)RAND_MAX;
			ListaEsferas[j]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
			ListaEsferas[j]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
		}


		ptr_shared_mem->esfera = *ListaEsferas[0];	//just to i = 0 because we desactivated the multi-ball functionality
		ptr_shared_mem->raqueta = jugador2;


		ptr_shared_mem->esfera = *ListaEsferas[0];
		ptr_shared_mem->raqueta = jugador2;
		switch(ptr_shared_mem->accion){
			case 0:	OnKeyboardDown('i', 0,0); break;
			case 1: OnKeyboardDown('o', 0, 0); break; // the last 2 params indicate the mouse position, dispensable in this case
			case -1: OnKeyboardDown('l', 0, 0); break;
			default: break;
		}

		if(aux != puntos1+puntos2 ){ //change in points
					points_flag = true;	//activate flag for sending data to the logger
					if(puntos1!=0 &&	puntos2!=0){
						if((puntos1 + puntos2)%3 == 0)	// every 3 points the balls get smaller
							ListaEsferas[j]->Shrink();

							/*if((puntos1 + puntos2)%5 == 0)	// every 5 points appears a new ball
							ListaEsferas.push_back(new Esfera());*/
					}
				}
}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
		case 'w':	jugador1.velocidad.y=4;	break;
		case 's':	jugador1.velocidad.y=-4;	break;
		case 'd': jugador1.velocidad.y = 0; break;

		case 'o':	jugador2.velocidad.y=4;	break;
		case 'l':	jugador2.velocidad.y=-4;	break;
		case 'i': jugador2.velocidad.y = 0; break;

		case 'x': ListaDisparos.push_back( new Disparo(jugador1.centro, 1));
							break;
		case 'p': ListaDisparos.push_back( new Disparo(jugador2.centro, 2));
							break;


	}
}

void CMundo::Init()
{
	ListaEsferas.push_back(new Esfera());
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	// shared memory

	int fd_sm;

	if((fd_sm = open("/tmp/shared_memory.txt", O_CREAT |  O_RDWR | O_TRUNC, 0777)) < 0 ){
		perror("Unable to open/create the shared memory file");
		exit(1);
	}

	char buffer[100];
	int n;

	void *sm;

	if((n = write(fd_sm, &shared_mem, sizeof(shared_mem))) < 0){	//create a file with the size of the allocated memory
		perror("Unable to write the sphere's center");
	}

	if ((sm = (char*)mmap((void*)0, sizeof(shared_mem), PROT_WRITE | PROT_READ, MAP_SHARED, fd_sm, 0)) == MAP_FAILED){	//mapping file in memory
		perror("Error projecting the file to memory");
		exit(1);
	}

	close(fd_sm);

	ptr_shared_mem = (DatosMemCompartida*)sm;	//assining the addres of the mapped file


}
