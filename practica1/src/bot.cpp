#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main(int argc, char const *argv[]) {

  DatosMemCompartida* shared_memory;
  char *dir;
  int fd;
  float sphere_center, player_center;
  void *mem_ptr;
  struct stat bstat;

  if ((fd = open("/tmp/shared_memory.txt", O_RDWR)) < 0){
    perror("Error opening the allocated file. Launch tenis first");
    return 1;
  }
  printf("opened\n");

  fstat(fd, &bstat);

  if ((mem_ptr = (char *)mmap((void*)0, 128, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0)) == MAP_FAILED){	//mapping file in memory
		perror("Error allocating the file to memory");
    return 1;
	}
  printf("mapped\n");

  close(fd);

  shared_memory = (DatosMemCompartida*)mem_ptr;	//assining the addres of the mapped file

  printf("assigned\n");

  while(1){
    sphere_center = shared_memory->esfera.centro.y;
    player_center = shared_memory->raqueta.centro.y;

    if(player_center > sphere_center)
      shared_memory->accion = -1;

    else if(player_center < sphere_center)
      shared_memory->accion = 1;

    else
      shared_memory->accion = 0;

    printf("player: %f  |  ball:  %f  |  action: %i  \n",player_center, sphere_center, shared_memory->accion);
    usleep(50000);
  }
  return 0;
}
