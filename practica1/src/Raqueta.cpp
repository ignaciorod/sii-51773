// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{

}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1 = y1 + velocidad.y * t;
	y2 = y2 + velocidad.y * t;

	centro.x = (x1 + x2) / 2;
	centro.y = (y1 + y2) / 2;
}

void Raqueta::Shrink(){
	if(sqrt(y1*y1 + y2*y2) > 0.66){ //once it's hit 5 times it shall shrink no more
	y1*=0.8;
	y2*=0.8;
	}
}
