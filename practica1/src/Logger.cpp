// program for reading from the FIFO

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>
using namespace std;

int fd1;
char *my_fifo = "/tmp/myfifo";  //al reiniciar el ordenador este archivo ya no estara

void treat_signal(int s){ //handling the SIGINT signal for closing the fifo
    if (close (fd1) == -1)
      perror ("close");
    unlink(my_fifo);
    exit(0);
}

int main(int argc, char const *argv[]) {
  struct sigaction action;
  char *data[80];

  if(mkfifo(my_fifo, 0666) < 0){ // create FIFO in /tmp
    perror("Error creating the FIFO");
    return 1;
  }

  action.sa_handler = &treat_signal;
  action.sa_flags = NULL;
  sigaction(SIGINT, &action, NULL);

  while(1){ //reads in a loop the data from tenis and prints it

    if((fd1 = open(my_fifo, O_RDONLY)) < 0){  //opening FIFO for reading from tenis
      perror("Error opening of the FIFO for reading");
      exit(1);
    }
    
    if(read(fd1, data, 100) == -1){
      cout<<"no writers"<<endl;
    }

    printf("%s", data);

    if (close (fd1) == -1) {
    perror ("close");
    break;
    }

  }

  unlink(my_fifo);
  return 0;
}
