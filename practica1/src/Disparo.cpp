#include <Disparo.h>
#include "glut.h"

// obtener posicion de raqueta para posicion inicial
Disparo::Disparo(Vector2D pos, int i)
{
  id = i;
  radio = 0.2f;
  centro = pos;
  switch (i){
    case 1: velocidad.x = 4;  break;
    default: velocidad.x = -4; break;
  }
  velocidad.y = 0;
}

Disparo::~Disparo()
{

}

int Disparo::getID(){
  return id;
}

void Disparo::Dibuja(){
  glColor3ub(250,100,0);
  glEnable(GL_LIGHTING);
  glPushMatrix();
  glTranslatef(centro.x,centro.y,0);
  glutSolidSphere(radio,15,15);
  glPopMatrix();
}
