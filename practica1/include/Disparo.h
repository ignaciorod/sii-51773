
#include "Esfera.h"
#include "Vector2D.h"

class Disparo: public Esfera
{
public:

  uint id;

  Disparo(Vector2D pos, int i);
  virtual ~Disparo();
  void Dibuja();
  
  int getID();
};
