// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "Raqueta.h"
#if _MSC_VER > 1000

#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Disparo.h"
#include "DatosMemCompartida.h"

#pragma once

class CMundo
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();


	Esfera esfera;
	std::vector<Plano> paredes;
	std::vector<Disparo *> ListaDisparos;
	std::vector<Esfera *> ListaEsferas;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	DatosMemCompartida shared_mem;
	DatosMemCompartida *ptr_shared_mem;

	int puntos1;
	int puntos2;
	bool points_flag;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
